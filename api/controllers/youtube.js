'use strict';

var util = require('util');
var youtube = require('../models/youtube');

function getChannelsForUser(req, res) {
  var username = req.swagger.params.username.value;
  youtube.getChannelsForUser(username).then(function (response) {
    res.json(response);
  });
}

function getPlaylists(req, res) {
  var channelId = req.swagger.params.channelId.value;
  var query = req.swagger.params.query.value || null;
  var pageToken = req.swagger.params.pageToken.value || null;

  if (!!query)
    youtube.searchPlaylists(channelId, query, pageToken).then(function (response) {
      res.json(response);
    });

  else
    youtube.getPlaylists(channelId, pageToken).then(function (response) {
      res.json(response);
    });
}

function getPlaylistItems(req, res) {
  var pageToken = req.swagger.params.pageToken.value || null;
  var playlistId = req.swagger.params.playlistId.value;

  youtube.getPlaylistItems(playlistId, pageToken).then(function (response) {
    res.json(response);
  });
}

function searchVideos(req, res) {
  var channelId = req.swagger.params.channelId.value;
  var query = req.swagger.params.query.value || null;
  var pageToken = req.swagger.params.pageToken.value || null;

  youtube.searchVideos(channelId, query, pageToken).then(function (response) {
    res.json(response);
  });
};

module.exports = {
  getChannelsForUser: getChannelsForUser,
  getPlaylists: getPlaylists,
  getPlaylistItems: getPlaylistItems,
  searchVideos: searchVideos,
};

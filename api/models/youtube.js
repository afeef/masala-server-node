var google = require('googleapis');
var async = require('async');
var _ = require('lodash');
var RSVP = require('rsvp');
var moment = require('moment');
var youtube = google.youtube('v3');
var config = require('../../config/googleapis');

var type = {
  channel: 'channel',
  playlist: 'playlist',
  video: 'video',
};

var parts = [
  'snippet',
  'contentDetails',
  'fileDetails',
  'player',
  'processingDetails',
  'recordingDetails',
  'statistics',
  'status',
  'suggestions',
  'topicDetails',
];

function getChannelsForUser(username) {
  var promise = new RSVP.Promise(function (resolve, reject) {
    var apiKey = config.youtubeApiKey;
    var options = {
      auth: apiKey,
      part: 'snippet,contentDetails',
      forUsername: username,
    };

    youtube.channels.list(options, function (err, response) {
      if (!!err)
        reject(err);

      resolve(response);
    });
  });

  return promise;
}

function getPlaylists(channelId, pageToken, playlistIds) {
  var promise = new RSVP.Promise(function (resolve, reject) {
    var apiKey = config.youtubeApiKey;
    var options = {
      auth: apiKey,
      part: 'snippet,contentDetails',
      maxResults: 50,
    };

    if (!!channelId)
      options.channelId = channelId;
    if (!!pageToken)
      options.pageToken = pageToken;
    if (!!playlistIds)
      options.id = playlistIds.join();

    youtube.playlists.list(options, function (err, response) {
      if (!!err)
        reject(err);

      var playlists = _.map(response.items, function (item) {
        var playlist = item.snippet;
        playlist.id = item.id;
        playlist.kind = item.kind;
        playlist.etag = item.etag;
        playlist.thumbnail = item.snippet.thumbnails.standard;
        playlist.itemCount = item.contentDetails.itemCount;

        return _.omit(playlist, ['thumbnails']);
      });

      response.playlists = playlists;
      resolve(_.omit(response, ['items']));
    });
  });

  return promise;
}

function searchPlaylists(channelId, query, pageToken) {
  var promise = new RSVP.Promise(function (resolve, reject) {
    var apiKey = config.youtubeApiKey;
    var options = {
      auth: apiKey,
      part: 'snippet',
      channelId: channelId,
      maxResults: 50,
      q: query,
      type: type.playlist,
    };

    if (!!pageToken)
      options.pageToken = pageToken;

    youtube.search.list(options, function (err, response) {
      if (!!err)
        reject(err);

      var playlistIds = _.map(response.items, function (item) {
        return item.id.playlistId;
      });

      resolve(getPlaylists(null, pageToken, playlistIds));
    });
  });

  return promise;
};

function getPlaylistItems(playlistId, pageToken) {
  var promise = new RSVP.Promise(function (resolve, reject) {
    var apiKey = config.youtubeApiKey;
    var options = {
      auth: apiKey,
      playlistId: playlistId,
      part: 'snippet',
      maxResults: 50,
    };

    if (!!pageToken)
      options.pageToken = pageToken;

    youtube.playlistItems.list(options, function (err, response) {
      if (!!err)
        reject(err);

      var videos = _.map(response.items, function (item) {
        return {
          id: item.snippet.resourceId.videoId,
          thumbnail: item.snippet.thumbnails.standard,
          kind: item.kind,
          etag: item.etag,
          publishedAt: moment(item.snippet.publishedAt).fromNow(),
          channelId: item.snippet.channelId,
          title: item.snippet.title,
          description: item.snippet.description,
          channelTitle: item.snippet.channelTitle,
          playlistId: item.snippet.playlistId,
        };
      });

      var videoIds = _.map(videos, 'id');

      var options = {
        auth: apiKey,
        part: 'statistics,contentDetails',
        id: videoIds.join(),
      };

      youtube.videos.list(options, function (err, videoStats) {

        _(videos).forEach(function (video) {
          var videoStat = _.find(videoStats.items, function (stat) {
            return video.id === stat.id;
          });

          video.duration = moment.duration(videoStat.contentDetails.duration).asSeconds();
          video.stats = videoStat.statistics;
        });

        response.videos = videos;
        resolve(_.omit(response, ['items']));
      });
    });
  });

  return promise;
}

function getVideos(videoIds) {

  var promise = new RSVP.Promise(function (resolve, reject) {
    var apiKey = config.youtubeApiKey;
    var videos = _.map(playlist.items, function (item) {

      var video = item.snippet;
      video.id = item.id;
      video.videoId = video.resourceId.videoId;
      video.thumbnail = video.thumbnails.standard;

      return _.omit(video, ['thumbnails', 'resourceId']);
    });

    var videoIds = _.map(videos, 'videoId');

    var options = {
      auth: apiKey,
      part: 'statistics',
      id: videoIds.toString(),
    };

    youtube.videos.list(options, function (err, videoStats) {

      _(videos).forEach(function (video) {
        var videoStat = _.find(videoStats.items, function (stat) {
          return video.videoId === stat.id;
        });

        video.stats = videoStat.statistics;
      });

      resolve(videos);
    });
  });

  return promise;
}

function searchVideos(channelId, query, pageToken) {
  var promise = new RSVP.Promise(function (resolve, reject) {
    var apiKey = config.youtubeApiKey;
    var options = {
      auth: apiKey,
      part: 'snippet',
      channelId: channelId,
      maxResults: 50,
      q: query,
      type: type.video,
    };

    if (!!pageToken)
      options.pageToken = pageToken;

    youtube.search.list(options, function (err, response) {
      if (!!err)
        reject(err);

      var videos = _.map(response.items, function (item) {
        return {
          id: item.id.videoId,
          thumbnail: item.snippet.thumbnails.high,
          kind: item.kind,
          etag: item.etag,
          publishedAt: moment(item.snippet.publishedAt).fromNow(),
          channelId: item.snippet.channelId,
          title: item.snippet.title,
          description: item.snippet.description,
          channelTitle: item.snippet.channelTitle,
        };
      });

      var videoIds = _.map(videos, 'id');

      var options = {
        auth: apiKey,
        part: 'statistics,contentDetails',
        id: videoIds.join(),
      };

      youtube.videos.list(options, function (err, videoStats) {

        _(videos).forEach(function (video) {
          var videoStat = _.find(videoStats.items, function (stat) {
            return video.id === stat.id;
          });

          video.duration = moment.duration(videoStat.contentDetails.duration).asSeconds();
          video.stats = videoStat.statistics;
        });

        response.videos = videos;
        resolve(_.omit(response, ['items']));
      });
    });
  });

  return promise;
};

module.exports = {
  getChannelsForUser: getChannelsForUser,
  getPlaylists: getPlaylists,
  searchPlaylists: searchPlaylists,
  getPlaylistItems: getPlaylistItems,
  searchVideos: searchVideos,
};
